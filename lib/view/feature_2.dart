import 'package:flutter/material.dart';

class FeatureScreenView2 extends StatelessWidget {
  const FeatureScreenView2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Feature Screen 2 '),
        ),
        body: Center(
          child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('DashBoard')),
        ));
  }
}
