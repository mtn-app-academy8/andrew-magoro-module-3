import 'package:flutter/material.dart';
import 'package:module_3/view/feature_1.dart';
import 'package:module_3/view/feature_2.dart';
import 'package:module_3/view/login_view.dart';
import 'package:module_3/view/profile_view.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  State<DashboardView> createState() => _DashboardState();
}

class _DashboardState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(25),
                    child: MaterialButton(
                      height: 25,
                      minWidth: 100,
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginView()),
                        );
                      },
                      splashColor: Colors.redAccent,
                      child: const Text('Logout'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(25),
                    child: MaterialButton(
                      height: 25,
                      minWidth: 100,
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const FeatureScreenView1()),
                        );
                      },
                      splashColor: Colors.redAccent,
                      child: const Text('Feature Screen 1'),
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(25),
                    child: MaterialButton(
                      height: 25,
                      minWidth: 100,
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const FeatureScreenView2()),
                        );
                      },
                      splashColor: Colors.redAccent,
                      child: const Text('Feature Screen 2'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(25),
                    child: MaterialButton(
                      height: 25,
                      minWidth: 100,
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ProfileView()),
                        );
                      },
                      splashColor: Colors.redAccent,
                      child: const Text('Profile'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
