import 'package:flutter/material.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  /* The main function that is build function returns widgets of the 
  widget data types and builds widgets starting from the scaffold widget
  with the properties appBar, which in itself has a widget AppBar that is a
  stateful widget and contains title widget, which in its self is has a const 
  text widget that is a stateless widget with the parameters registration */
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Profile View'),
        ),
        body: Column(children: [
          const TextField(
            enableSuggestions: false,
            autocorrect: false,
            keyboardType: TextInputType.name,
            decoration: InputDecoration(hintText: 'Enter Username Here'),
          ),
          const TextField(
            enableSuggestions: false,
            autocorrect: false,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(hintText: 'Enter Cell Number Here'),
          ),
          ElevatedButton(
            onPressed: () {},
            child: const Text('Save'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Dashboard'),
          ),
        ]));
  }
}
